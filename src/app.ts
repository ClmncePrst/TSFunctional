/*
Les paramètres à prendre en compte:
- Tous les colis partent de France => pas d'envoi entre Japon/Canada directement
    (est-ce que la conversion CAD/JPY est pertinente du coup?)
- Frais de douane : pas besoin d'appliquer pour le Japon vu que n'atteint jamais 5000 JPY
    France -> Canada : +15% pour colis de plus de 1kg
    Si colis reste en France: pas de frais supplémentaires
- Cas de figure:
    - France à France:
        'EUR': { '1kg': 10, '3kg': 20, 'above3kg': 30 },
        'EUR': { 'above150cm': 5 },
    - France à Canada:
        1 EUR = 1.5 CAD & 1 CAD = 0.67 EUR,
        'CAD': { '1kg': 15, '3kg': 30, 'above3kg': 45 },
        'CAD': { 'above150cm': 7.5 },
        Si >1kg ou CAD20, prix = prix+15% 
    - France à Japon:
        1 EUR = 130 JPY &  1 JPY = 0.0077 EUR,
        'JPY': { '1kg': 1000, '3kg': 2000, 'above3kg': 3000 },
        'JPY': { 'above150cm': 500 }, 
*/
// Convertisseur de devises entre l'euro, le dollar canadien et le yen
let convertAmount = (amount: number, initialCurrency: string, finalCurrency: string) => {
    if (initialCurrency === 'EUR') {
        return finalCurrency === 'CAD' ? `${amount * 1.5} CAD` : `${amount * 130} JPY`;
    } else if (initialCurrency === 'CAD') {
        return finalCurrency === 'EUR' ? `${amount * 0.67} EUR` : `${amount * 87} JPY`;
    } else {
        return finalCurrency === 'EUR' ? `${amount * 0.0077} EUR` : `${amount * 0.0115} CAD`;
    }
};
console.log(convertAmount(10, 'EUR', 'CAD'));
console.log(convertAmount(100, 'JPY', 'EUR'));
console.log(convertAmount(50, 'CAD', 'JPY'));

// Calcul des frais de livraison à partir du poids du colis, avec des frais supplémentaires si les dimensions du colis sont > à 150cm
let shippingRates = (weight: number, dimensions: { length: number; width: number; height: number }, country: string) => {
    // Calcul des dimensions total du colis pour éventuellement appliquer des frais supplémentaires fixes par pays
    const totalDimensions = dimensions.length + dimensions.width + dimensions.height;
    let rate = 0;

    if (totalDimensions > 150) {
        rate += country === 'France' ? 5 : country === 'Canada' ? 7.5 : country === 'Japon' ? 500 : 0;
    }
    // Calcul des frais de livraison pour un colis pesant moins de 1kg
    if (weight <= 1) {
        rate += country === 'France' ? 10 : country === 'Canada' ? 15 : country === 'Japon' ? 1000 : 0;
        // Calcul des frais de livraison pour un colis pesant moins de 3kg
    } else if (weight <= 3) {
        rate += country === 'France' ? 20 : country === 'Canada' ? 30 : country === 'Japon' ? 2000 : 0;
        // Calcul des frais de livraison pour un colis pesant plus de 3kg
    } else {
        rate += country === 'France' ? 30 : country === 'Canada' ? 45 : country === 'Japon' ? 3000 : 0;
    }
    return rate;
};
console.log(shippingRates(3, { length: 51, width: 50, height: 65 }, 'Japon'));
console.log(shippingRates(1, { length: 10, width: 8, height: 25 }, 'France'));
console.log(shippingRates(3, { length: 50, width: 50, height: 50 }, 'Canada'));


// Calcul des frais de douane, en fonction de la somme déclarée du colis, sachant que tous les colis partent de France
let calculateCustomFees = (country: string, declaredValue: number) => {
    let customFees = 0;
    // si le colis part au Canada et à une valeur de plus de 20CAD, ajouter une taxe de 15%
    if (country === 'Canada' && declaredValue > 20) {
        customFees = declaredValue * 0.15;
        // si le colis part au Japon et à une valeur de plus de 5000 JPY, ajouter une taxe de 10%
    } else if (country === 'Japon' && declaredValue > 5000) {
        customFees = declaredValue * 0.10;
        // si le colis reste en France, aucun frais supplémentaire ne sera appliqué
    } else {
        console.log('Pas de frais supplémentaires');
    }
    return customFees;
};
console.log(calculateCustomFees('Canada', 50));
console.log(calculateCustomFees('Japon', 7500));
console.log(calculateCustomFees('France', 75));