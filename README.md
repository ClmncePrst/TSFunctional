# Installer et exécuter TypeScript

## Prérequis
Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre machine :

Node.js : Un environnement d'exécution JavaScript.
npm : Le gestionnaire de packages pour Node.js.

## Installation
### Initialiser NPM

Créer un fichier **package.json** avec la commande 
```
npm init -y
```
### Ajouter la dépendance TypeScript pour Node
```
npm i —save-dev typescript @types/node
```

##  Configuration
Pour configurer TypeScript, il faut tout d’abord créer un fichier de configuration **tsconfig.json** qui définit les options et fonctionnalités de TypeScript.Il est aussi possible de l’effectuer automatiquement grâce à la commande
```
 npx tsc —init
```

## Exécution


### Compilation du code TypeScript
Utilisez la commande suivante pour compiler le code TypeScript en JavaScript
```
npx tsc 
```

### Exécution de l'application
Après compilation, exécutez l'application avec Node.JS
```
node dist/app.js 
```
### Exécution en mode développement
Pour lancer notre application sans passer par la compilation, il est possible d’utiliser **ts-node** qui exécute directement le code TypeScript. 
Il faut tout d’abord installer **ts-node** en tant que dépendance de développement
```
npm i --save-dev ts-node
```
Puis, exécuter le code TypeScript directement avec **ts-node**
````
npx ts-node src/app.ts
```
