"use strict";
console.log("hello");
/*
Les paramètres à prendre en compte:
- Tous les colis partent de France => pas d'envoi entre Japon/Canada directement
    (est-ce que la conversion CAD/JPY est pertinente du coup?)
- Frais de douane : pas besoin d'appliquer pour le Japon vu que n'atteint jamais 5000 JPY
    France -> Canada : +15% pour colis de plus de 1kg
    Si colis reste en France: pas de frais supplémentaires


- Cas de figure:
    - France à France:
        'EUR': { '1kg': 10, '3kg': 20, 'above3kg': 30 },
        'EUR': { 'above150cm': 5 },
    - France à Canada:
        1 EUR = 1.5 CAD & 1 CAD = 0.67 EUR,
        'CAD': { '1kg': 15, '3kg': 30, 'above3kg': 45 },
        'CAD': { 'above150cm': 7.5 },
        Si >1kg ou CAD20, prix = prix+15%
    - France à Japon:
        1 EUR = 130 JPY &  1 JPY = 0.0077 EUR,
        'JPY': { '1kg': 1000, '3kg': 2000, 'above3kg': 3000 },
        'JPY': { 'above150cm': 500 },
*/
const convertAmount = (amount, initialCurrency, finalCurrency) => {
    if (initialCurrency === 'EUR') {
        return finalCurrency === 'CAD' ? `${amount * 1.5} CAD` : `${amount * 130} JPY`;
    }
    else if (initialCurrency === 'CAD') {
        return finalCurrency === 'EUR' ? `${amount * 0.67} EUR` : `${amount * 87} JPY`;
    }
    else {
        return finalCurrency === 'EUR' ? `${amount * 0.0077} EUR` : `${amount * 0.0115} CAD`;
    }
};
console.log(convertAmount(100, 'EUR', 'CAD'));
